import './assets/main.css';

import { createApp, provide } from 'vue';
import axios from 'axios';
import App from './App.vue';
import * as FunctionHelpers from './helpers/functions.js';
import * as VariableHelpers from './helpers/variables.js';

const globalVariables = {
	vite: `Vite`,
	vue: `Vue Js 3`,
};

const app = createApp(App);

// Provide global variables and functions to use in all of components
app.provide('globalVariables', globalVariables);
app.provide('VariableHelpers', VariableHelpers);
app.provide('FunctionHelpers', FunctionHelpers);

app.mount('#app');
