import toastr from 'toastr';
import 'toastr/build/toastr.css';

export function greet() {
	toastr.info(`Hello Developer!!!...`);
}

// Validation Functions
export function hasNoEmptyInputs(inputs){
	let hasNoEmptyInputs = true;
	for (const key in inputs) {
		const value = inputs[key];
		if(value === '' || value === null){
			hasNoEmptyInputs = false;
			toastr.error(`${key} must not be empty!`);
		}
	}
	return hasNoEmptyInputs;
}
