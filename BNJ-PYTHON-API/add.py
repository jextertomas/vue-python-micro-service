from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)

# This functions is doing the addition
@app.route('/arithmethic', methods=['POST'])
def arithmethic():
	data = request.json
	total = int(data['num_one']) + int(data['num_two'])
	result = {"total": total}
	return jsonify(result)

if __name__ == '__main__':
    app.run(debug=True)

