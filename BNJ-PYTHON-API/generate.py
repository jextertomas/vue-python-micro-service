import openpyxl
from openpyxl.drawing.image import Image
from PIL import Image as PILImage
from openpyxl.utils import get_column_letter
from flask import Flask, request, jsonify, send_file, make_response
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)

@app.route('/process_data', methods=['POST'])
def process_data():
    data = request.form.to_dict()
    data.update({'file': request.files['file']})
    digest(data)
    result = {"message": "Data is successfully recieved.", "result": "success"}
    return jsonify(result)


@app.route('/api/data', methods=['GET'])
def get_data():
    # Process your data and prepare it to be sent as a JSON response
    data = {'result': 'your_backend_data'}
    return jsonify(data)

@app.route('/download_file')
def download_file():
	# Create a response with the file data
    response = make_response(send_file('template_1.xlsx', as_attachment=True))

    # Set the Content-Disposition header to trigger the file download
    response.headers['Content-Disposition'] = 'attachment; filename=template_1.xlsx'

    # Return the response
    return response

# function for setting value inside the excel file
def digest(data):
	template = openpyxl.load_workbook('temp.xlsx')

	sheetData = template['SheetA'] 

	array_replace = {"VARIABLE_A" : data['va'], "VARIABLE_B" : data['vb'], "VARIABLE_C" : data['vc'], "VARIABLE_D" : data['vd']}


	image_file_path = data['file']
	img = PILImage.open(image_file_path)
	img_width, img_height = img.size

	# Specify the desired width and height for the image in Excel (in pixels)
	desired_width = 100
	desired_height = 200
	param = "VARIABLE_IMG_A"
	cell_image = ''


	for row in sheetData.iter_rows():
		for cell in row:
			cell_value = cell.value
			if cell_value in array_replace:
				cell.value = array_replace[cell_value]
			elif param in str(cell.value):
				exp1 = cell_value.split()
				exploded = exp1[2].split(':')
				desired_width = int(exploded[0])
				desired_height = int(exploded[1])
				cell.value = ""
				cell_image = get_column_letter(cell.column) + str(cell.row)



	# Calculate the scaling factors for width and height
	width_scale = desired_width / img_width
	height_scale = desired_height / img_height

	# Create an Image object from the image file and adjust its size
	img_obj = Image(image_file_path)
	img_obj.width = int(img_width * width_scale)
	img_obj.height = int(img_height * height_scale)

	# Add the adjusted image to the worksheet
	sheetData.add_image(img_obj, cell_image)



	template.save('template_1.xlsx')


if __name__ == '__main__':
    app.run(debug=True)
